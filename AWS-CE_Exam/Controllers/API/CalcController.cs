﻿using System;
using Microsoft.AspNetCore.Mvc;
using AWS_CE_Exam.Services;
using System.Collections.Generic;
using System.Linq;

namespace AWS_CE_Exam.Controllers
{
	[Route("/api/[controller]")]
	public class CalcController : Controller
	{
		private readonly ICalcService calcService;

		public CalcController(ICalcService calcService)
		{
			this.calcService = calcService;
		}

		// GET http://~/api/calc?
		[HttpGet]
		public string Get()
		{
			/// QueryString => ?hoge (Length = 5)
			if (!Request.QueryString.HasValue || Request.QueryString.Value.Length <= 1) {
				return "ERROR";
			}

			/// Skip first char, ?
			return calcService.SimpleFourOperations(new String(Request.QueryString.Value.Skip(1).ToArray()));
		}
	}
}
