﻿using System;
using Microsoft.AspNetCore.Mvc;
using AWS_CE_Exam.Services;
using System.Collections.Generic;
using System.Linq;

namespace AWS_CE_Exam.Controllers
{
	[Route("/api/[controller]")]
	public class StockerController : Controller
	{
        private readonly IStockerService stockerService;

		public StockerController(IStockerService stockerService)
		{
            this.stockerService = stockerService;
		}

		// GET http://~/api/stocker?function=
		[HttpGet]
		public string Get(string function)
		{
            switch (function)
            {
                case "addstock":
                    return stockerService.AddStock(Request.Query);
                case "checkstock":
                    return stockerService.CheckStock(Request.Query);
                case "sell":
                    return stockerService.Sell(Request.Query);
                case "checksales":
                    return stockerService.CheckSales(Request.Query);
                case "deleteall":
                    return stockerService.DeleteAll(Request.Query);
                default:
                    return "UNDEFINED";
            }
		}
	}
}
