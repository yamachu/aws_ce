﻿using Microsoft.AspNetCore.Mvc;

namespace AWS_CE_Exam.Controllers
{
	[Route("[controller]")]
	public class SecretController : Controller
	{
		// GET http://~/secret/
		[HttpGet]
		public string Get()
		{
			return "SUCCESS";
		}
	}
}
