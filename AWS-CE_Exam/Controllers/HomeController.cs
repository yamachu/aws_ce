﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AWS_CE_Exam.Controllers
{
	[Route("/")]
	public class HomeController : Controller
	{
		// GET http://~/
		[HttpGet]
		public string Get()
		{
			return "AMAZON";
		}
	}
}
