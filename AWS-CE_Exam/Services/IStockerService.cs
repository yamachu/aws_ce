﻿using System;
using Microsoft.AspNetCore.Http;

namespace AWS_CE_Exam.Services
{
	public interface IStockerService
	{
        string AddStock(IQueryCollection query);
        string CheckStock(IQueryCollection query);
        string Sell(IQueryCollection query);
        string CheckSales(IQueryCollection query);
        string DeleteAll(IQueryCollection query);
	}
}
