﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace AWS_CE_Exam.Services
{
	public class CalcService : ICalcService
	{
		/* ToDo: 文字列リソースはどこかしらにまとめる */
		private const string STR_ERROR = "ERROR";

		public string SimpleFourOperations(string formula)
		{
			if (!IsRestrictCharacter(formula))
			{
				return STR_ERROR;
			}
			if (ContainsInvalidOperator(formula))
			{
				return STR_ERROR;
			}

			var tokens = InfixToTokens(formula);
			if (!ContainsNumber(tokens))
			{
				return STR_ERROR;
			}

			try
			{
				var rpnTokens = TokensToRPN(tokens);
                return CaclulateRPN(rpnTokens).ToString();
			}
			catch (Exception ex)
			{
				return STR_ERROR;
			}
		}

		private bool IsRestrictCharacter(string formula)
			=> Regex.Match(formula, @"^[+\-*\/\(\)0-9]+$").Success;

		/// <summary>
		/// Validate operator, example: ++, --, +-, ...
		/// </summary>
		/// <returns><c>true</c>, if invalid operator was containsed, <c>false</c> otherwise.</returns>
		/// <param name="formula">Formula.</param>
		private bool ContainsInvalidOperator(string formula)
			=> Regex.Match(formula, @"^.*[+\-*\/][+\-*\/].*$").Success;

		private bool ContainsNumber(List<string> tokens)
			=> tokens.Any(s => !(s.Equals("+")
							  || s.Equals("-")
					      || s.Equals("*")
					      || s.Equals("/")
					      || s.Equals("(")
				      || s.Equals(")")));

		/// <summary>
		/// Infix string to number and operator tokens.
		/// Currently NOT SUPPORT minus value
		/// </summary>
		/// <returns>Number and Operator token string list</returns>
		/// <param name="infix">Infix notation</param>
		private List<string> InfixToTokens(string infix)
		{
			var infixArr = infix.ToCharArray();
			var tmpList = new List<string>();
			var resultTokens = new List<string>();
			var isAfterFirstToken = true;
			var willMinusComing = false;

			for (var i = 0; i < infix.Length; i++)
			{
				if ("+-*/()".Contains(infixArr[i]))
				{
					if (infixArr[i] == '-' && isAfterFirstToken)
					{
						willMinusComing = true;
						continue;
					}
					isAfterFirstToken = false;

					if (infixArr[i] == '(')
					{
						isAfterFirstToken = true;
					}

					if (tmpList.Count > 0)
					{
						resultTokens.Add((willMinusComing ? "-" : "")
										 + tmpList.Aggregate((first, second) => first + second));
						tmpList.Clear();
						willMinusComing = false;
					}

					/* 左の項に - がついた場合に呼ばれるけど，対応難しい気がする．
					 * 1+1+(-(3-2)+2)
					 * みたいな形式．
					 * 意味論的には変わってしまうが， 0- と置き換えることで対応
					 */
					if (willMinusComing)
					{
						willMinusComing = false;
						resultTokens.Add("0");
						resultTokens.Add("-");
					}
					resultTokens.Add(infixArr[i].ToString());
				}
				else
				{
                    isAfterFirstToken = false;
					tmpList.Add(infixArr[i].ToString());
				}
			}
			if (tmpList.Count > 0)
			{
				resultTokens.Add((willMinusComing ? "-" : "")
						 + tmpList.Aggregate((first, second) => first + second));
				tmpList.Clear();
			}

			return resultTokens;
		}

		private int OperatorPriority(string token)
		{
			if (token.Equals("*") || token.Equals("/"))
			{
				return 2;
			}
			else if (token.Equals("+") || token.Equals("-"))
			{
				return 1;
			}
			else // is Number
			{
				return 0;
			}
		}

		private bool IsNumber(string token)
			=> int.TryParse(token, out _);

		/// <summary>
		/// Tokenses to rpn.
		/// </summary>
		/// <see cref="http://home.a00.itscom.net/hatada/c-tips/rpn/rpn02.html"/>
		/// <seealso cref="http://7ujm.net/etc/calcstart.html"/>
		/// <returns>The to rpn.</returns>
		/// <param name="tokens">Tokens.</param>
		private List<string> TokensToRPN(List<string> tokens)
		{
			var stack = new Stack<string>();
			var rpnList = new List<string>();

			foreach (string s in tokens)
			{
				/* 数字はそのまま積んでいく */
				if (IsNumber(s))
				{
					rpnList.Add(s);
					continue;
				}

				if (s.Equals("("))
				{
					stack.Push(s);
					continue;
				}
				if (s.Equals(")"))
				{
					/* 一つの括弧のペアの中身を吐き出す */
					while (stack.Count != 0 && !stack.Peek().Equals("("))
					{
                        rpnList.Add(stack.Pop());
					}
                    stack.Pop();
					continue;
				}

				/* 演算子の優先度に応じて，同じ優先度もしくは低い優先度になるまで吐き出す */
				while (stack.Count != 0 && OperatorPriority(stack.Peek()) >= OperatorPriority(s))
				{
					rpnList.Add(stack.Pop());
				}
				stack.Push(s);
			}

			/* 終端まで来て残っていたら全部吐き出す */
			while (stack.Count != 0)
			{
				rpnList.Add(stack.Pop());
			}

			return rpnList;
		}

		private int CaclulateRPN(List<string> rpnList)
		{
			var stack = new Stack<int>();
			var tmp = 0;

			foreach (var s in rpnList)
			{
				if (int.TryParse(s, out tmp))
				{
					stack.Push(tmp);
					continue;
				}

				switch (s)
				{
					case "+":
						{
							var b = stack.Pop();
							var a = stack.Pop();
							stack.Push(a + b);
							break;
						}
					case "-":
						{
							var b = stack.Pop();
							var a = stack.Pop();
							stack.Push(a - b);
							break;
						}
					case "*":
						{
							var b = stack.Pop();
							var a = stack.Pop();
							stack.Push(a * b);
							break;
						}
					case "/":
						{
							var b = stack.Pop();
							var a = stack.Pop();
							stack.Push(a / b);
							break;
						}
					default:
						break;
				}
			}

            return stack.Pop();
		}
	}
}
