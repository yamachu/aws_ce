﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace AWS_CE_Exam.Services
{
    public class StockerService : IStockerService
	{
		/* ToDo: 文字列リソースはどこかしらにまとめる */
		private const string STR_ERROR = "ERROR";
	
        public string AddStock(IQueryCollection query)
        {
            StringValues tmp;
            string name;
            int amount = 1;

            if (!query.TryGetValue("name", out tmp)) {
                return STR_ERROR;
            }
            name = tmp.ToString();
            if (name.Length > 8) {
                return STR_ERROR;
            }

            if (query.TryGetValue("amount", out tmp))
                {
                    if (!int.TryParse(tmp.ToString(), out amount))
                    {
                        return STR_ERROR;
                    }
                    if (amount < 1)
                    {
                        return STR_ERROR;
                    }
                }

            // Do Add

            return "";
        }

        public string CheckStock(IQueryCollection query)
        {
            StringValues tmp;
            string name = "";

            if (query.TryGetValue("name", out tmp)) {
                name = tmp.ToString();
            }

            if (name.Length > 0) {
				if (name.Length > 8)
				{
					return STR_ERROR;
				}
                // check this
                // return name: (amount?.Count > 0 ? count : 0)
            } else {
                // check all
                // return sorted name and (amount > 0)
            }

            return "RESULT";
        }

        public string Sell(IQueryCollection query)
        {
			StringValues tmp;
			string name;
			int amount = 1;
            double price = double.NaN;

			if (!query.TryGetValue("name", out tmp))
			{
				return STR_ERROR;
			}
			name = tmp.ToString();
            if (name.Length > 8)
			{
				return STR_ERROR;
			}

			if (query.TryGetValue("amount", out tmp))
			{
				if (!int.TryParse(tmp.ToString(), out amount))
				{
					return STR_ERROR;
				}
				if (amount < 1)
				{
					return STR_ERROR;
				}
			}

            if (query.TryGetValue("price", out tmp)) {
                if (!double.TryParse(tmp.ToString(), out price)) 
                {
                    return STR_ERROR;
                }
                if (price < 0)
                {
                    return STR_ERROR;
                }
            }

            // 在庫の数のチェック ないなら STR_ERROR
            // 在庫から数を引く
            // price がついていたら売上に加算 price is not NaN

            return "";
        }

        public string CheckSales(IQueryCollection query)
        {
            // return sales result $"sales: {0:##}"
            return "100";
        }

        public string DeleteAll(IQueryCollection query)
        {
            // Drop table
            return "";
        }
    }
}
