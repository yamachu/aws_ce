﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace AWS_CE_Exam.Middlewares
{
	/// <summary>
	/// Basic authentication middleware.
	/// </summary>
	/// <see cref="http://stackoverflow.com/questions/38977088/asp-net-core-web-api-authentication"/>
	public class BasicAuthenticationMiddleware
	{
		private readonly RequestDelegate _next;

		public BasicAuthenticationMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext context)
		{
			string authHeader = context.Request.Headers["Authorization"];
			if (authHeader != null && authHeader.StartsWith("Basic"))
			{
				//Extract credentials
				string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
				Encoding encoding = Encoding.GetEncoding("iso-8859-1");
				string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

				int seperatorIndex = usernamePassword.IndexOf(':');

				var username = usernamePassword.Substring(0, seperatorIndex);
				var password = usernamePassword.Substring(seperatorIndex + 1);

				if (username == "amazon" && password == "candidate")
				{
					await _next.Invoke(context);
				}
				else
				{
					context.Response.StatusCode = 401; //Unauthorized
					return;
				}
			}
			else
			{
				// no authorization header
				context.Response.StatusCode = 401; //Unauthorized
				return;
			}
		}
	}
}
